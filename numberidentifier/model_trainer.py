#!/usr/bin/env python3
# Data Augmentation Resource: https://www.tensorflow.org/tutorials/images/data_augmentation

USE_WANDB = True

import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten, Conv2D, RandomRotation, RandomTranslation, RandomZoom
from tensorflow.keras import Model
import numpy as np
import wandb
import datetime
import matplotlib.pyplot as plt

## Load data into memory
# Using the MNIST dataset and normalizing 0-255 range to 0-1
mnist = tf.keras.datasets.mnist
(image_train, label_train), (image_test, label_test) = mnist.load_data()
image_train, image_test = image_train / 255.0, image_test / 255.0


# Add a channels dimension
image_train = image_train[..., tf.newaxis].astype("float32")
image_test = image_test[..., tf.newaxis].astype("float32")

train_ds = tf.data.Dataset.from_tensor_slices((image_train, label_train)).shuffle(10000).batch(32)
test_ds = tf.data.Dataset.from_tensor_slices((image_test, label_test)).batch(32)


## Preprocessing layer
# Slap into the model as a preprocessing step
# TODO: Probably needs more work...
data_augment = tf.keras.Sequential([
  RandomZoom((-0.3, -0.15)),
  RandomRotation(0.2),
  RandomTranslation(height_factor=(-0.1, 0.1), width_factor=(-0.1, 0.1))
])


## The ML model
# Has 4 layers to work with
class NumberIdentifier(Model):
  def __init__(self):
    super(NumberIdentifier, self).__init__()
    self.data_aug = data_augment
    self.conv1 = Conv2D(32, 3, activation='relu')
    self.flatten = Flatten()
    self.d1 = Dense(128, activation='relu')
    self.d2 = Dense(10)

  def call(self, x):
    x = self.data_aug(x)
    x = self.conv1(x)
    x = self.flatten(x)
    x = self.d1(x)
    return self.d2(x)

number_identifier = NumberIdentifier()


## Pre-training
# Optimizer and loss function
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
optimizer = tf.keras.optimizers.Adam()

# Metrics to measure the loss and accuracy of the model (These metrics accumulate over all epochs)
train_loss = tf.keras.metrics.Mean(name='train_loss')
train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

test_loss = tf.keras.metrics.Mean(name='test_loss')
test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='test_accuracy')


## Training step
# Track stuff we need
@tf.function							#This is called a "function decorator" in python
def train_step(images, labels):
  with tf.GradientTape() as tape:				#Use tf.GradientTape() to train
    predictions = number_identifier(images, training=True)
    loss = loss_object(labels, predictions)
  gradients = tape.gradient(loss, number_identifier.trainable_variables)
  optimizer.apply_gradients(zip(gradients, number_identifier.trainable_variables))

  train_loss(loss)
  train_accuracy(labels, predictions)


## Testing step
# Track stuff we need
@tf.function
def test_step(images, labels):
  predictions = number_identifier(images, training=False)
  t_loss = loss_object(labels, predictions)

  test_loss(t_loss)
  test_accuracy(labels, predictions)


## Train the model and save if this file is run
if __name__ == '__main__':

  time_now = datetime.datetime.today().strftime("%Y_%m_%d_%H_%M_%S")
  EPOCHS = 50

  # WanDB stuff
  if USE_WANDB:
    wandb.login()
    wandb.init(
      project='kevin_numberthingy',
      name='kevin_test' + time_now,
      entity='oscarunt',
      config={
        "Model": number_identifier,
        "Time": time_now,
        "Number epochs": EPOCHS
      }
    )
    wandb.config
  
  for epoch in range(EPOCHS):
    # Reset the metrics at the start of the next epoch
    train_loss.reset_states()
    train_accuracy.reset_states()
    test_loss.reset_states()
    test_accuracy.reset_states()
  
    for images, labels in train_ds:
      train_step(images, labels)
  
    for test_images, test_labels in test_ds:
      test_step(test_images, test_labels)
  
    print(
      f'Epoch {epoch + 1}, '
      f'Loss: {train_loss.result()}, '
      f'Accuracy: {train_accuracy.result() * 100}, '
      f'Test Loss: {test_loss.result()}, '
      f'Test Accuracy: {test_accuracy.result() * 100}'
    )
    
    if USE_WANDB:
      wandb.log({
        'loss': train_loss.result(),
        'accuracy': train_accuracy.result() * 100,
        'test_loss': test_loss.result(),
        'test_accuracy': test_accuracy.result() * 100
      })
  
  # Save the Model
  number_identifier.save('numberidentifier_model')
  
  # Finish WanDB stuff
  if USE_WANDB:
    wandb.finish(exit_code=None, quiet=None)
