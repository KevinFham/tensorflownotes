#!/usr/bin/env python3
import tensorflow as tf
import tkinter as tk
from PIL import Image, ImageGrab
import numpy as np

number_identifier = tf.keras.models.load_model('numberidentifier_model', compile=False)    #Import the model
prediction_model = tf.keras.Sequential([number_identifier, tf.keras.layers.Softmax()])  #Convert imported model into prediction model

## Tkinter Interface
WINDOW_MAXSIZE_X = 700
WINDOW_MAXSIZE_Y = 700

class DrawingWindow(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.m1_press = False
        self.canvas = tk.Canvas(self, width=WINDOW_MAXSIZE_X+1, height=WINDOW_MAXSIZE_Y+1, background="black", cursor="pencil")
        self.canvas.pack(side="top", fill="both", expand=True)
        self.canvas.bind("<ButtonPress-1>", self.on_m1_press)   
        self.canvas.bind("<ButtonRelease-1>", self.on_m1_release)
        self.canvas.bind("<Motion>", self.motion)
        self.canvas.bind("<ButtonPress-3>", self.on_m2_press)

    def on_m1_press(self, event):
        self.m1_press = True

    def on_m1_release(self, event):
        global image
        self.m1_press = False

        # Take image out of canvas
        image = ImageGrab.grab(bbox=(self.canvas.winfo_rootx()+1, self.canvas.winfo_rooty()+1, self.canvas.winfo_rootx() + WINDOW_MAXSIZE_X+1, self.canvas.winfo_rooty() + WINDOW_MAXSIZE_Y+1))
        image_arr = np.array(image)
        image_predict = np.array([[j[0] for j in i] for i in image_arr])
        
        # Preprocess the image
        image_predict = np.array([i[::25] for i in image_predict[::25]])        #Compress 700x700 to 28x28
        image_predict = image_predict / 255.0                                   #Normalize
        image_predict = np.array([image_predict])                               #Place image into array for formatting
        image_predict = image_predict[...,tf.newaxis].astype("float32")         #Insert third channel for formatting
        #output = Image.fromarray(image_predict)
        #output.show()

        # Predict the image
        predictions = prediction_model.predict(image_predict)
        print("Prediction: ", np.argmax(predictions[0]))

    def motion(self, event):
        if self.m1_press:
            x = event.x
            y = event.y
            self.canvas.create_oval(x-20,y-20,x+20,y+20, fill="white", outline="white")

    def on_m2_press(self, event):
        self.canvas.create_rectangle(0,0,WINDOW_MAXSIZE_X,WINDOW_MAXSIZE_Y, fill="black", outline="black")


if __name__ == "__main__":
    window = DrawingWindow()
    window.mainloop()
