#!/usr/bin/env python3
#https://www.tensorflow.org/tutorials/distribute/keras
import tensorflow as tf
import tensorflow_datasets as tfds
import os


## Distributed Training

# The tf.distribute.Strategy API assists with distributing training across multiple processing units
# This program will mainly use .MirroredStrategy to train a model on multiple GPUs on one machine (more specifics in documentation)
strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

# Load Dataset (with_info=True includes metadata of entire set, which is saved into @info)
datasets, info = tfds.load(name='mnist', with_info=True, as_supervised=True)
mnist_train, mnist_test = datasets['train'], datasets['test']


#(Cannot continue due to posession only one GPU for the time being)
