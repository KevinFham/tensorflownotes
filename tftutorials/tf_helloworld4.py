#!/usr/bin/env python3
#https://www.tensorflow.org/tutorials/load_data/csv#mixed_data_types
import tensorflow as tf
from tensorflow.keras import layers
import pandas as pd
import numpy as np

np.set_printoptions(precision=3, suppress=True)         # Numpy Config



## Loading CSV data (Mixed data types)

# The "Titanic" dataset contains information about the passengers on the Titanic
titanic = pd.read_csv("https://storage.googleapis.com/tf-datasets/titanic/train.csv")
titanic.head()						#Download into a pandas DataFrame, though not immediately usable as input to a model

# The task is to predict who survived on the Titanic
titanic_features = titanic.copy()
titanic_labels = titanic_features.pop('survived')



## Preprocessing (in depth explanation)

# Each column now holds different data types of different ranges. Each one needs to be handled seperately
#  It is possible to turn all columns into numerical values, but all data input into the model must then be preprocessed
#  Keras preprocessing avoid this since they are part of the model
input = tf.keras.Input(shape=(), dtype=tf.float32)		#Create symbolic input
result = 2*input + 1						#Perform calculation on the input
print("Input Calc: ", result)					#View result

# Keras functional API operates on "symbolic" tensors which keep track of what operations are run on them, rather than normal "eager" tensors which have a value
calc = tf.keras.Model(inputs=input, outputs=result)
print("2 * 1 + 1: ", calc(1).numpy())				#"Remembers" the operation performed in @result
print("2 * 2 + 1: ", calc(2).numpy())



## Preprocessing (actual)

# Create a preprocessing model, building a set of symbolic tf.keras.Input objects that match the names/datatypes of the CSV
inputs = {}
for name, column in titanic_features.items():
  dtype = column.dtype
  if dtype == object:
    dtype = tf.string
  else:
    dtype = tf.float32

  inputs[name] = tf.keras.Input(shape=(1,), name=name, dtype=dtype)	#Create the tf.keras.Input object with the column's name and datatype
print("Preprocessing Model: ", inputs)

# Concatenate all numeric inputs together and run through a normalization layer
numeric_inputs = {name:input for name,input in inputs.items()
                  if input.dtype==tf.float32}

x = layers.Concatenate()(list(numeric_inputs.values()))			#Concatenate
norm = layers.Normalization()						#Create normalization layer
norm.adapt(np.array(titanic[numeric_inputs.keys()]))			#Normalize input
all_numeric_inputs = norm(x)

print("Preprocessed Numerical Input:", all_numeric_inputs)

preprocessed_inputs = [all_numeric_inputs]				#Collect all symbolic preprocessing results to concatenate later

# For string inputs, things must be handled differently
for name, input in inputs.items():
  if input.dtype == tf.float32:
    continue

  lookup = layers.StringLookup(vocabulary=np.unique(titanic_features[name]))		#tf.keras.layers.StringLookup() will map strings to integer indices in a vocabulary
  one_hot = layers.CategoryEncoding(num_tokens=lookup.vocabulary_size())		#tf.keras.layers.CategoryEncoding() converts indexes to float32
												#Default settings for the CategoryEncoding() layer create a one-hot vector for each input
  x = lookup(input)
  x = one_hot(x)
  preprocessed_inputs.append(x)								#Append to numeric inputs

# Concatenate all the preprocessed inputs together and build a model that handles preprocessing
preprocessed_inputs_cat = layers.Concatenate()(preprocessed_inputs)

titanic_preprocessing = tf.keras.Model(inputs, preprocessed_inputs_cat)

#tf.keras.utils.plot_model(model = titanic_preprocessing , rankdir="LR", dpi=72, show_shapes=True)	#Needs pydot and graphviz to work; diagram presented in the referred tutorial URL

# Keras models don't automatically convert pandas DataFrame; must be specified to use either one tensor or a dictionary of tensors
# This time, we need a dictionary of tensors
titanic_features_dict = {name: np.array(value)
                         for name, value in titanic_features.items()}

# View the preprocessed input after passing the first training example
features_dict = {name:values[:1] for name, values in titanic_features_dict.items()}
print("Preprocessed Input:", titanic_preprocessing(features_dict))



## Building the Model

# Combine preprocessing model and ML model into one function
def titanic_model(preprocessing_head, inputs):
  body = tf.keras.Sequential([
    layers.Dense(64),
    layers.Dense(1)
  ])

  preprocessed_inputs = preprocessing_head(inputs)
  result = body(preprocessed_inputs)
  model = tf.keras.Model(inputs, result)

  model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                optimizer=tf.keras.optimizers.Adam())
  return model

titanic_model = titanic_model(titanic_preprocessing, inputs)		#Here is the finished model



## Training

#Features are X, labels are Y
titanic_model.fit(x=titanic_features_dict, y=titanic_labels, epochs=10)

# Saving and Reloading the model
titanic_model.save('test')
reloaded = tf.keras.models.load_model('test')

before = titanic_model(features_dict)
after = reloaded(features_dict)
assert (before-after)<1e-3
print("Original Model:", before)
print("Saved/Reloaded Model:", after)

