#!/usr/bin/env python3
# https://www.tensorflow.org/tutorials/generative/style_transfer

import os
import tensorflow as tf
os.environ['TFHUB_MODEL_LOAD_FORMAT'] = 'COMPRESSED'		# Load compressed models from tensorflow_hub
import numpy as np
import PIL.Image
import time
import functools
import IPython.display as display
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['figure.figsize'] = (12, 12)
mpl.rcParams['axes.grid'] = False

