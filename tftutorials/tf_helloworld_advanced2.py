#!/usr/bin/env python3
#https://www.tensorflow.org/tutorials/customization/basics
import tensorflow as tf					#Note: As of TF2, eager execution is turned on by default, which enables a more interactive frontend
import numpy as np
import time

## Tensors

# Tensors are multidimensional arrays (variable (0D) > array (1D) > matrix (2D) > tensor (3D)). They can reside in accelerator memory, such as a GPU
# TensorFlow provides many basic operations that consume/produce tf.Tensor objects
print(tf.math.add(1, 2))
print(tf.math.add([1, 2], [3, 4]))
print(tf.math.square(5))
print(tf.math.reduce_sum([1, 2, 3]))
print(tf.math.square(2) + tf.math.square(3))		#Operator overloading is also supported

# Each tf.Tensor has a shape and datatype
x = tf.linalg.matmul([[1]], [[2, 3]])
print(x)
print(x.shape)
print(x.dtype)

# Unlike NumPy arrays, tf.Tensor objects can be backed by the GPU and are immutable
# However, a NumPy ndarray can be converted to a tf.Tensor and vice versa automatically
ndarr = np.ones([3, 3])
tensor = tf.math.multiply(ndarr, 42)			#Automatically convert numpy to tensor
print(tensor)

print(np.add(tensor, 1))				#Numpy operations convert tensor to numpy
print(tensor.numpy())					#Explicitly convert tensor to numpy



## GPU Acceleration

# Without any annotations, TensorFlow automatically decides whether to use GPU or CPU for an operation, copying the tensor between CPU and GPU memory if necessary
# The Tensor.device property provides a name of the device hosting the tensor's contents (GPU:<N> notates that the tensor is placed on the N-th GPU of the host)
print(tf.config.list_physical_devices("CPU"))
print(tf.config.list_physical_devices("GPU"))
print(x.device)

# TensorFlow operations can be explicitly placed onto specific devices
def time_matmul(x):
    start = time.time()
    for loop in range(10):
        tf.linalg.matmul(x, x)
    result = time.time()-start

    print("10 loops: {:0.2f}ms".format(1000*result))

with tf.device("CPU:0"):				# Force CPU execution
    print("CPU:")
    x = tf.random.uniform([1000, 1000])
    assert x.device.endswith("CPU:0")
    time_matmul(x)

if tf.config.list_physical_devices("GPU"):
    print("GPU:")
    with tf.device("GPU:0"):
        x = tf.random.uniform([1000, 1000])
        assert x.device.endswith("GPU:0")
        time_matmul(x)
