#!/usr/bin/env python3
#https://www.tensorflow.org/tutorials/load_data/csv
import tensorflow as tf
from tensorflow.keras import layers
import pandas as pd
import numpy as np

np.set_printoptions(precision=3, suppress=True) 	# Numpy Config


## Loading CSV data

# The Abalone dataset contains measurements of different Abalone, a type of sea snail
abalone_train = pd.read_csv(
    "https://storage.googleapis.com/download.tensorflow.org/data/abalone_train.csv",
    names=["Length", "Diameter", "Height", "Whole weight", "Shucked weight",
           "Viscera weight", "Shell weight", "Age"])
abalone_train.head()								#Download into a pandas 'DataFrame'

# The task is to predict the age of an Abalone through other measurements
abalone_features = abalone_train.copy()
abalone_labels = abalone_features.pop('Age')

# For now, all features are treated identically (no weights). Pack into a numpy array
abalone_features = np.array(abalone_features)
print("Abalone Data: ", abalone_features)


## Preprocessing

# Its good practice to normalize the inputs to the model. This is a basic way to normalize the measurements given by Keras preprocessing
normalize = layers.Normalization()
normalize.adapt(abalone_features)


## Building the Model

# This model will predict the age. Since there is only a single input tensor, a keras.Sequential model is sufficient
abalone_model = tf.keras.Sequential([
  #(Types of layers to use; this one uses 3)
  normalize,			#Using the normalization layer done in preprocessing
  layers.Dense(64),
  layers.Dense(1)
])

# Configure and compile
abalone_model.compile(
  loss = tf.keras.losses.MeanSquaredError(),
  optimizer = tf.keras.optimizers.Adam()
)


## Training

# Pass features and labels; this is the most basic way to train a model using CSV data
abalone_model.fit(abalone_features, abalone_labels, epochs=10)

