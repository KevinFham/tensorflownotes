#!/usr/bin/env python3
# https://www.tensorflow.org/tutorials/generative/data_compression
# Note: matplotlib may not work, but visuals are present in tutorial as well
import tensorflow as tf
import tensorflow_compression as tfc
import tensorflow_datasets as tfds
import matplotlib.pyplot as plt


'''
 Learned Data Compression
'''
# Using neural networks to perform lossy data compression on MNIST dataset
# The model will act like an audoencoder, and will therefore need to perform different functions during training and inference:


# Analysis (encoder) transform converts the image into a latent space
def make_analysis_transform(latent_dims):
  """Creates the analysis (encoder) transform."""
  return tf.keras.Sequential([
      tf.keras.layers.Conv2D(20, 5, use_bias=True, strides=2, padding="same", activation="leaky_relu", name="conv_1"),
      tf.keras.layers.Conv2D(50, 5, use_bias=True, strides=2, padding="same", activation="leaky_relu", name="conv_2"),
      tf.keras.layers.Flatten(),
      tf.keras.layers.Dense(500, use_bias=True, activation="leaky_relu", name="fc_1"),
      tf.keras.layers.Dense(latent_dims, use_bias=True, activation=None, name="fc_2"),
  ], name="analysis_transform")


# Synthesis (decoder) transform converts latent space back into image
def make_synthesis_transform():
  """Creates the synthesis (decoder) transform."""
  return tf.keras.Sequential([
      tf.keras.layers.Dense(500, use_bias=True, activation="leaky_relu", name="fc_1"),
      tf.keras.layers.Dense(2450, use_bias=True, activation="leaky_relu", name="fc_2"),
      tf.keras.layers.Reshape((7, 7, 50)),
      tf.keras.layers.Conv2DTranspose(20, 5, use_bias=True, strides=2, padding="same", activation="leaky_relu", name="conv_1"),
      tf.keras.layers.Conv2DTranspose(1, 5, use_bias=True, strides=2, padding="same", activation="leaky_relu", name="conv_2"),
  ], name="synthesis_transform")
  
  
# The model that will hold these functions is the prior and entropy model, which models the marginal probabilities of the latents
# The call() is where the rate and distortion are computed
# The rate is the expected number of bits needed to encode a sample and the distortion is the expected error in the sample's reconstruction 
class MNISTCompressionTrainer(tf.keras.Model):
  """Model that trains a compressor/decompressor for MNIST."""

  def __init__(self, latent_dims):
    super().__init__()
    self.analysis_transform = make_analysis_transform(latent_dims)
    self.synthesis_transform = make_synthesis_transform()
    self.prior_log_scales = tf.Variable(tf.zeros((latent_dims,)))

  @property
  def prior(self):
    return tfc.NoisyLogistic(loc=0., scale=tf.exp(self.prior_log_scales))

  def call(self, x, training):
    """Computes rate and distortion losses."""
    # Ensure inputs are floats in the range (0, 1).
    x = tf.cast(x, self.compute_dtype) / 255.
    x = tf.reshape(x, (-1, 28, 28, 1))

    # Compute latent space representation y, perturb it and model its entropy,
    # then compute the reconstructed pixel-level representation x_hat.
    y = self.analysis_transform(x)
    entropy_model = tfc.ContinuousBatchedEntropyModel(self.prior, coding_rank=1, compression=False)
    y_tilde, rate = entropy_model(y, training=training)
    x_tilde = self.synthesis_transform(y_tilde)

    # Average number of bits per MNIST digit.
    rate = tf.reduce_mean(rate)

    # Mean absolute difference across pixels.
    distortion = tf.reduce_mean(abs(x - x_tilde))

    return dict(rate=rate, distortion=distortion)
    
    
    
'''
 Analyzing Step by Step
'''

# Load MNIST dataset and extract one image for analyzing
training_dataset, validation_dataset = tfds.load("mnist", split=["train", "test"], shuffle_files=True, as_supervised=True, with_info=False,)
(x, _), = validation_dataset.take(1)
print(f"Data type: {x.dtype}")
print(f"Shape: {x.shape}")
plt.imshow(tf.squeeze(x))			#Image before compression

# Convert to latent representation 'y', which requires casting to float32, adding a batch dimension, and passing through analysis transform
x = tf.cast(x, tf.float32) / 255.
x = tf.reshape(x, (-1, 28, 28, 1))
y = make_analysis_transform(10)(x)
print("y:", y)					#Latent

# Latents are quantized at test time. Modelling this in a differentiable way during training requires adding uniform noise between (-.5, .5)
y_tilde = y + tf.random.uniform(y.shape, -.5, .5)
print("y_tilde:", y_tilde)			#Latent with noise

# The "prior" is a probability density trained for modelling the marginal distribution of the noisy latents
# tfc.NoisyLogistic() accounts for latents with additive noise
prior = tfc.NoisyLogistic(loc=0., scale=tf.linspace(.01, 2., 10))
_ = tf.linspace(-6., 6., 501)[:, None]
plt.plot(_, prior.prob(_));			#Logistic distribution visualization to show that the noise produces a "noisy" distrubution that approaches uniform distribution

# tfc.ContinuousBatchedEntropyModel() adds uniform noise, using the noise and the 'prior' to compute a (differentiable) upper bound on the rate
# This bound can be minimized as a loss
entropy_model = tfc.ContinuousBatchedEntropyModel(prior, coding_rank=1, compression=False)
y_tilde, rate = entropy_model(y, training=True)
print("rate:", rate)
print("y_tilde:", y_tilde)

# Now, the noisy latents can be passed through the synthesis transform to reconstruct the image
# The transform is currently untrained, so the distortion (error between original and reconstructed image) is very high
x_tilde = make_synthesis_transform()(y_tilde)
distortion = tf.reduce_mean(abs(x - x_tilde))
print("distortion:", distortion)

x_tilde = tf.saturate_cast(x_tilde[0] * 255, tf.uint8)
print(f"Data type: {x_tilde.dtype}")
print(f"Shape: {x_tilde.shape}")
plt.imshow(tf.squeeze(x_tilde))			#Reconstructed image


# For every batch of digits, calling MNISTCompressionTrainer produces rate and distortion as an average over the batch
# Training the model will lessen these two losses
(example_batch, _), = validation_dataset.batch(32).take(1)
trainer = MNISTCompressionTrainer(10)
example_output = trainer(example_batch)
print("rate: ", example_output["rate"])
print("distortion: ", example_output["distortion"])



'''
 Training the Model
'''
# We want the model to optimize the rate-distortion Lagrangian (a sum of rate and distortion, with one of the terms being weighted by a Lagrange parameter (looks like a lambda))
# The loss function used will affect different parts of the model:
# - Analysis transform trained to achieve the desired rate/distortion trade-off
# - Synthesis transform is trained to minimize distortion based on the latent
# - Parameters of the 'prior' trained to minimize rate based on latent

def pass_through_loss(_, x):
  return x					#Rate and distortion are unsupervised; loss doesn't need a target

def make_mnist_compression_trainer(lmbda, latent_dims=50):
  trainer = MNISTCompressionTrainer(latent_dims)
  trainer.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
    loss=dict(rate=pass_through_loss, distortion=pass_through_loss),		#Just pass rate and distortion as losses/metrics.
    metrics=dict(rate=pass_through_loss, distortion=pass_through_loss),
    loss_weights=dict(rate=1., distortion=lmbda),
  )
  return trainer
  
  
# Now train the model. Training is unsupervised, so labels aren't necessary

def add_rd_targets(image, label):
  return image, dict(rate=0., distortion=0.)	#Labels arent necessary, but still need to add "dummy" targets for rate and distortion

def train_mnist_model(lmbda):
  trainer = make_mnist_compression_trainer(lmbda)
  trainer.fit(training_dataset.map(add_rd_targets).batch(128).prefetch(8),epochs=15, validation_data=validation_dataset.map(add_rd_targets).batch(128).cache(), validation_freq=1, verbose=1,)
  return trainer

trainer = train_mnist_model(lmbda=2000)		#Rate-distortion tradeoff value of 2000. Can be messed with to train for different tradeoffs
						#A tradeoff value of 500 reduces the rate, but increases the distortion. A tradeoff of 300 increases these effects



'''
 Compressing MNIST images
'''
# Trained model will be split into an encoder side (w/ analysis transform & entropy model) and decoder side (w/ synthesis transform & same entropy model)
# At test time, latents will not have additive noise, but are quantized and losslessly compressed, so x_hat and y_hat will represent the latents and the reconstruction

class MNISTCompressor(tf.keras.Model):
  """Compresses MNIST images to strings."""

  def __init__(self, analysis_transform, entropy_model):
    super().__init__()
    self.analysis_transform = analysis_transform
    self.entropy_model = entropy_model

  def call(self, x):
    x = tf.cast(x, self.compute_dtype) / 255.
    y = self.analysis_transform(x)
    _, bits = self.entropy_model(y, training=False)				#Also return exact information content of each digit
    return self.entropy_model.compress(y), bits
    
class MNISTDecompressor(tf.keras.Model):
  """Decompresses MNIST images from strings."""

  def __init__(self, entropy_model, synthesis_transform):
    super().__init__()
    self.entropy_model = entropy_model
    self.synthesis_transform = synthesis_transform

  def call(self, string):
    y_hat = self.entropy_model.decompress(string, ())
    x_hat = self.synthesis_transform(y_hat)
    return tf.saturate_cast(tf.round(x_hat * 255.), tf.uint8)			#Scale and cast back to 8-bit integer
    

# The entropy model converts the learned 'prior' into tables for a range coding algorithm (compression=True)
# This algorithm is invoked when calling compress(), converting the latent space vector into bit sequences
# The entropy model instance must be identical for the compressor and decompressor for proper decoding

def make_mnist_codec(trainer, **kwargs):
  entropy_model = tfc.ContinuousBatchedEntropyModel(trainer.prior, coding_rank=1, compression=True, **kwargs)
  compressor = MNISTCompressor(trainer.analysis_transform, entropy_model)
  decompressor = MNISTDecompressor(entropy_model, trainer.synthesis_transform)
  return compressor, decompressor

compressor, decompressor = make_mnist_codec(trainer)


# Grab and compress 16 images from the validation dataset (subset 3) into strings
(originals, _), = validation_dataset.batch(16).skip(3).take(1)
strings, entropies = compressor(originals)
print(f"String representation of first digit in hexadecimal: 0x{strings[0].numpy().hex()}")
print(f"Number of bits actually needed to represent it: {entropies[0]:0.2f}")


# Decompress images back from strings
# Note that the length of the encoded string differs for the information content of eeach digit
reconstructions = decompressor(strings)

def display_digits(originals, strings, entropies, reconstructions):
  """Visualizes 16 digits together with their reconstructions."""
  fig, axes = plt.subplots(4, 4, sharex=True, sharey=True, figsize=(12.5, 5))
  axes = axes.ravel()
  for i in range(len(axes)):
    image = tf.concat([
        tf.squeeze(originals[i]),
        tf.zeros((28, 14), tf.uint8),
        tf.squeeze(reconstructions[i]),
    ], 1)
    axes[i].imshow(image)
    axes[i].text(
        .5, .5, f"→ 0x{strings[i].numpy().hex()} →\n{entropies[i]:0.2f} bits",
        ha="center", va="top", color="white", fontsize="small",
        transform=axes[i].transAxes)
    axes[i].axis("off")
  plt.subplots_adjust(wspace=0, hspace=0, left=0, right=1, bottom=0, top=1)
  
display_digits(originals, strings, entropies, reconstructions)



'''
 Using Decoder as a Generative Model
'''
# The decoder has learned to turn controlled noise into a digit. If we feed the decoder random noise, it will try to generate numbers on its own
# (Related concept: https://www.tensorflow.org/tutorials/generative/generate_images_with_stable_diffusion)

# The compressor and decompressor need to be instantiated without a sanity check, which would detect if the input string was not fully decoded
compressor, decompressor = make_mnist_codec(trainer, decode_sanity_check=False)

#Then, feed long enough random strings into the decompressor to generate digits
import os

strings = tf.constant([os.urandom(8) for _ in range(16)])
samples = decompressor(strings)

fig, axes = plt.subplots(4, 4, sharex=True, sharey=True, figsize=(5, 5))
axes = axes.ravel()
for i in range(len(axes)):
  axes[i].imshow(tf.squeeze(samples[i]))
  axes[i].axis("off")
plt.subplots_adjust(wspace=0, hspace=0, left=0, right=1, bottom=0, top=1)	#Generated numbers




