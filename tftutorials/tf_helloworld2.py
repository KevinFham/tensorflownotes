#!/usr/bin/env python3
#https://www.tensorflow.org/tutorials/keras/classification
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
print("TF Vers: ", tf.__version__)


## Loading

# Fashion MNIST dataset contains 70k images (28x28) of grayscale clothing items, of which there are 10 categories
fashion_mnist = tf.keras.datasets.fashion_mnist

# 60k images are for training, 10k are for testing
# Each image in each dataset has a label, resulting in four arrays
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

print("Training Set: ", train_images.shape)
print("Training Labels: ", train_labels, " ", train_labels.shape)
print("Testing Set: ", test_images.shape)
print("Testing Labels: ", test_labels, " ", test_labels.shape)

# Normalize the images' 0-255 range to 0-1
train_images = train_images / 255.0
test_images = test_images / 255.0

# Labels range from 0-9, corresponding to this array:
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']


## Viewing

# Display image in the training set
#plt.figure()
#plt.imshow(train_images[0])
#plt.colorbar()
#plt.grid(False)
#plt.show()

# Display the first 25 images with corresponding labels
#plt.figure(figsize=(10,10))
#for i in range(25):
#    plt.subplot(5,5,i+1)
#    plt.xticks([])
#    plt.yticks([])
#    plt.grid(False)
#    plt.imshow(train_images[i], cmap=plt.cm.binary)
#    plt.xlabel(class_names[train_labels[i]])
#plt.show()


## Training

# Set up model
model = tf.keras.Sequential([
    #(Types of layers to use; this model uses 3)
    tf.keras.layers.Flatten(input_shape=(28, 28)),		#Turns 2 dimensional array into 1 dimensional array of 784 pixels
    tf.keras.layers.Dense(128, activation='relu'),		#Densely connected neural layers; this one has 128 nodes/neurons
    tf.keras.layers.Dense(10)					#This layer returns a logits array of length 10, corresponding to one of the 10 categories of clothing
])

# Before training, configure and compile
model.compile(
    optimizer='adam',								#Specify how the model updates based on the data it sees and its loss function
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),	#Measures how accurate the model is during training
    metrics=['accuracy']							#Specify what to monitor during training and testing
)

# Train the model
model.fit(train_images, train_labels, epochs=10)

# Evaluate model with the test dataset
test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('Test accuracy:', test_acc)


## Making Predictions

# Convert model's linear outputs (logits) to probabilities, then predict the labels of each image in the test dataset
probability_model = tf.keras.Sequential([model,
                                         tf.keras.layers.Softmax()])
predictions = probability_model.predict(test_images)

# Predictions (an array of size 10) of the first test image
# These 10 predictions represent the model's confidence in the image's correspondence to each of the 10 clothing categories
print("Predictions of TestImg1: ", predictions[0])
print("Predicted Label of TestImg1: ", class_names[np.argmax(predictions[0])])
print("Actual Label of TestImg1: ", class_names[test_labels[0]])


