#!/usr/bin/env python3
# https://www.tensorflow.org/tutorials/quickstart/beginner
import tensorflow as tf
print("TF Vers: ", tf.__version__)


## Loading

# Load MNIST dataset
mnist = tf.keras.datasets.mnist

# Dataset pixels range from 0-255
# Normalize to a range btwn 0-1 (divide by 255.0)
(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# Build a Sequential ML model
model = tf.keras.models.Sequential([
  # (Types of layers; this model uses 4)
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation='softmax')
])


## Viewing

# View model as a numpy array
predictions = model(x_train[:1]).numpy()
print("Predictions: ", predictions)

# Convert to probabilities and view
print("Probabilities: ", tf.nn.softmax(predictions).numpy())


## Training

# Define a loss function and print out the initial loss before training
# (This is due to fairly random probabilities)
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
print("Initial Loss (before training): ", loss_fn(y_train[:1], predictions).numpy())

# Configure and compile model before training
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Train Model
model.fit(x_train, y_train, epochs=5)

# Evaluate performance based on test set
model.evaluate(x_test, y_test)

# View trained model probabilities as an array
probability_model = tf.keras.Sequential([
  model,
  tf.keras.layers.Softmax()
])
print("New Probabilities: ", probability_model(x_test[:5]))
